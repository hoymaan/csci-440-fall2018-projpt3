.read data.sql

WITH
FriendshipsInvolvingCassandra AS (
	SELECT *
	FROM Friend
	WHERE ID1 IN (
		SELECT ID
		FROM Highschooler
		WHERE Name = 'Cassandra'
	) OR
	ID2 IN (
		SELECT ID
		FROM Highschooler
		WHERE Name = 'Cassandra'
	)
),
FriendIDsOfCassandra AS (
	SELECT DISTINCT ID
	FROM (
		SELECT ID1 AS ID
		FROM FriendshipsInvolvingCassandra
		UNION
		SELECT ID2 AS ID
		FROM FriendshipsInvolvingCassandra
	)
	WHERE ID NOT IN (
		SELECT ID
		FROM Highschooler
		WHERE Name = 'Cassandra'
	)
),
FriendshipsInvolvingFriendsOfCassandra AS (
	SELECT *
	FROM Friend
	WHERE ID1 IN (
		SELECT ID
		FROM FriendIDsOfCassandra
	) OR
	ID2 IN (
		SELECT ID
		FROM FriendIDsOfCassandra
	)
),
FriendsIDsOfFriendsOfCassandra AS (
	SELECT DISTINCT ID
	FROM (
		SELECT ID1 AS ID
		FROM FriendshipsInvolvingFriendsOfCassandra
		UNION
		SELECT ID2 AS ID
		FROM FriendshipsInvolvingFriendsOfCassandra
	)
	WHERE ID NOT IN (
		SELECT ID
		FROM FriendIDsOfCassandra
	) AND
	ID NOT IN (
		SELECT ID
		FROM Highschooler
		WHERE Name = 'Cassandra'
	)
),
DesiredIDs AS (
	SELECT ID
	FROM FriendIDsOfCassandra
	UNION
	SELECT ID
	FROM FriendsIDsOfFriendsOfCassandra
)
SELECT count(*)
FROM Highschooler
WHERE ID IN (
	SELECT ID
	FROM DesiredIDs
);