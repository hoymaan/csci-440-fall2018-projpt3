.read data.sql

-- 13. For every situation where student A likes student B, but we have no information about whom B likes (that is, B does not appear as an ID1 in the Likes table), return A and B's names and grades.

WITH LikesLonerIDs as (
	SELECT l1.ID1, l1.ID2 FROM Likes AS l1	
	LEFT JOIN Likes
	ON l1.ID2 = Likes.ID1
	WHERE Likes.ID1 is null
),
LikesLoner as (
	SELECT
	h1.name as name1, 
	h1.grade as grade1, 
	h2.name as name2, 
	h2.grade as grade2 
	FROM LikesLonerIds
	INNER JOIN Highschooler as h1
	ON LikesLonerIds.ID1 = h1.ID
	INNER JOIN Highschooler as h2
	ON LikesLonerIds.ID2 = h2.ID
	ORDER BY h1.name
)
SELECT * FROM LikesLoner;

