.read data.sql

-- Print before and after updates
select * from Friend;

WITH
TransientFriendshipCase1 AS (
	SELECT
		Friend1.ID2 AS ID1,
		Friend1.ID1 AS ID2,
		Friend2.ID2 AS ID3
	FROM Friend AS Friend1
	LEFT JOIN Friend AS Friend2
		ON Friend1.ID1 = Friend2.ID1
	WHERE
		Friend1.ID1 != Friend1.ID2 AND
		Friend1.ID2 != Friend2.ID2 AND
		Friend1.ID1 != Friend2.ID2
),
TransientFriendshipCase2 AS (
	SELECT
		Friend1.ID1 AS ID1,
		Friend1.ID2 AS ID2,
		Friend2.ID1 AS ID3
	FROM Friend AS Friend1
	LEFT JOIN Friend AS Friend2
		ON Friend1.ID2 = Friend2.ID2
	WHERE
		Friend1.ID1 != Friend1.ID2 AND
		Friend1.ID2 != Friend2.ID1 AND
		Friend1.ID1 != Friend2.ID1
),
TransientFriendshipCase3 AS (
	SELECT
		Friend1.ID1 AS ID1,
		Friend1.ID2 AS ID2,
		Friend2.ID2 AS ID3
	FROM Friend AS Friend1
	LEFT JOIN Friend AS Friend2
		ON Friend1.ID2 = Friend2.ID1
	WHERE
		Friend1.ID1 != Friend1.ID2 AND
		Friend1.ID2 != Friend2.ID2 AND
		Friend1.ID1 != Friend2.ID2
),
TransientFrienships AS (
	SELECT
		ID1,
		ID2,
		ID3
	FROM TransientFriendshipCase1
	UNION
	SELECT
		ID1,
		ID2,
		ID3
	FROM TransientFriendshipCase2
	UNION
	SELECT
		ID1,
		ID2,
		ID3
	FROM TransientFriendshipCase3
),
FriendshipsToAdd AS (
	SELECT DISTINCT
		ID1,
		ID2
	FROM TransientFrienships
	WHERE
		ID1 NOT IN (
			SELECT ID1
			FROM Friend
			WHERE Friend.ID2 = TransientFrienships.ID2
		) AND
		ID2 NOT IN (
			SELECT ID2
			FROM Friend
			WHERE Friend.ID1 = TransientFrienships.ID1
		)
)
INSERT INTO Friend (
	ID1,
	ID2
)
SELECT *
FROM FriendshipsToAdd;

select * from Friend;
