.read data.sql

WITH DistinctNames AS (
	SELECT DISTINCT
		ID,
		name
	FROM Highschooler
	GROUP BY name
	ORDER BY ID
)
SELECT count(*)
FROM Highschooler
LEFT JOIN DistinctNames
	ON DistinctNames.ID = Highschooler.ID
WHERE DistinctNames.ID IS NULL;