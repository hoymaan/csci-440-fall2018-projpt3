.read data.sql

WITH  MutualLikesIDs AS (
	SELECT
		Likes1.ID1 AS ID1,
		Likes1.ID2 AS ID2
	FROM Likes AS Likes1
	LEFT JOIN Likes AS Likes2
		ON Likes1.ID2 = Likes2.ID1
	WHERE Likes1.ID1 = Likes2.ID2
),
MutualLikes AS (
	SELECT
		Highschooler1.name AS Name1,
		Highschooler1.grade AS Grade1,
		Highschooler2.name AS Name2,
		Highschooler2.grade AS Grade2
	FROM MutualLikesIDs
	LEFT JOIN Highschooler AS Highschooler1
		ON MutualLikesIDs.ID1 = Highschooler1.ID
	LEFT JOIN Highschooler AS Highschooler2
		ON MutualLikesIDs.ID2 = Highschooler2.ID
)
SELECT
	Name1,
	Grade1,
	Name2,
	Grade2
FROM MutualLikes
WHERE Name1 < Name2;