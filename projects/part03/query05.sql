.read data.sql

WITH FriendsOfSameGrade AS (
	SELECT
		Friend.ID1 AS ID1,
		Friend.ID2 AS ID2,
		Person2.ID AS Grade
	FROM Friend
	LEFT JOIN Highschooler AS Person1
		ON Friend.ID1 = Person1.ID
	LEFT JOIN Highschooler AS Person2
		ON Friend.ID2 = Person1.ID
	WHERE Person1.Id = Person2.ID
)
SELECT
	Name,
	Grade
FROM Highschooler
WHERE ID NOT IN (
	SELECT ID1
	FROM FriendsOfSameGrade
	UNION
	SELECT ID2
	FROM FriendsOfSameGrade
);