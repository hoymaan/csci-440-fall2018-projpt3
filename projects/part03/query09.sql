.read data.sql

WITH IDsOfPeopleNamedGabrial AS (
	SELECT ID
	FROM Highschooler
	WHERE Name = 'Gabriel'
),
FriendshipsInvolvingAGabriel AS (
	SELECT *
	FROM Friend
	WHERE
		ID1 IN (
			SELECT ID
			FROM IDsOfPeopleNamedGabrial
		) OR
		ID2 IN (
			SELECT ID
			FROM IDsOfPeopleNamedGabrial
		)
),
IDsOfFriendsOfGabriel AS (
	SELECT ID1
	FROM FriendshipsInvolvingAGabriel
	WHERE ID2 IN (
		SELECT ID
		FROM IDsOfPeopleNamedGabrial
	)
	UNION
	SELECT ID2
	FROM FriendshipsInvolvingAGabriel
	WHERE ID1 IN (
		SELECT ID
		FROM IDsOfPeopleNamedGabrial
	)
)
SELECT
	Name,
	Grade
FROM Highschooler
WHERE ID IN (
	SELECT ID
	FROM IDsOfFriendsOfGabriel
);
