.read data.sql

WITH
LikesThatAreNotFriends AS (
	SELECT *
	FROM Likes
	WHERE
		ID1 NOT IN (
			SELECT ID1
			FROM Friend
			WHERE Likes.ID2 = ID2
		) AND
		ID1 NOT IN (
			SELECT ID2
			FROM Friend
			WHERE Likes.ID2 = ID1
		)
),
TransientFriendshipCase1 AS (
	SELECT
		Friend1.ID2 AS ID1,
		Friend1.ID1 AS ID2,
		Friend2.ID2 AS ID3
	FROM Friend AS Friend1
	LEFT JOIN Friend AS Friend2
		ON Friend1.ID1 = Friend2.ID1
	WHERE
		Friend1.ID1 != Friend1.ID2 AND
		Friend1.ID2 != Friend2.ID2 AND
		Friend1.ID1 != Friend2.ID2
),
TransientFriendshipCase2 AS (
	SELECT
		Friend1.ID1 AS ID1,
		Friend1.ID2 AS ID2,
		Friend2.ID1 AS ID3
	FROM Friend AS Friend1
	LEFT JOIN Friend AS Friend2
		ON Friend1.ID2 = Friend2.ID2
	WHERE
		Friend1.ID1 != Friend1.ID2 AND
		Friend1.ID2 != Friend2.ID1 AND
		Friend1.ID1 != Friend2.ID1
),
TransientFriendshipCase3 AS (
	SELECT
		Friend1.ID1 AS ID1,
		Friend1.ID2 AS ID2,
		Friend2.ID2 AS ID3
	FROM Friend AS Friend1
	LEFT JOIN Friend AS Friend2
		ON Friend1.ID2 = Friend2.ID1
	WHERE
		Friend1.ID1 != Friend1.ID2 AND
		Friend1.ID2 != Friend2.ID2 AND
		Friend1.ID1 != Friend2.ID2
),
TransientFrienships AS (
	SELECT
		ID1,
		ID2,
		ID3
	FROM TransientFriendshipCase1
	UNION
	SELECT
		ID1,
		ID2,
		ID3
	FROM TransientFriendshipCase2
	UNION
	SELECT
		ID1,
		ID2,
		ID3
	FROM TransientFriendshipCase3
),
TransientFriendshipsOfLikes AS (
	SELECT *
	FROM TransientFrienships
	WHERE
		ID1 IN (
			SELECT ID1
			FROM LikesThatAreNotFriends
			WHERE ID3 = LikesThatAreNotFriends.ID2
		)
)
SELECT
	Person1.name,
	Person1.grade,
	Person2.name,
	Person2.grade,
	Person3.name,
	Person3.grade
FROM TransientFriendshipsOfLikes
LEFT JOIN Highschooler AS Person1
	ON TransientFriendshipsOfLikes.ID1 = Person1.ID
LEFT JOIN Highschooler AS Person2
	ON TransientFriendshipsOfLikes.ID2 = Person2.ID
LEFT JOIN Highschooler AS Person3
	ON TransientFriendshipsOfLikes.ID2 = Person3.ID;