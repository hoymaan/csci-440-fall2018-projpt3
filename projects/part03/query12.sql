.read data.sql


WITH Lonely AS (

	SELECT grade, name
	FROM HIGHSCHOOLER
	WHERE ID NOT IN (
		SELECT ID1 
		FROM LIKES
		UNION 
		SELECT ID2
		FROM LIKES
	)
)

SELECT * FROM Lonely
	ORDER BY	
		grade ASC,
		name ASC;