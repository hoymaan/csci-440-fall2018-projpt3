.read data.sql

SELECT name, grade FROM Highschooler
INNER JOIN (SELECT ID2
	FROM Likes
	GROUP BY ID2
	HAVING count(*) > 1) as popular
ON Highschooler.ID = popular.ID2;
